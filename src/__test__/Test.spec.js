import React from "react";
import { create } from "react-test-renderer";
import Adapter from 'enzyme-adapter-react-16';
import { mount, shallow, configure } from 'enzyme';
import { MemoryRouter } from 'react-router-dom'
import Footer from "../Footer/Footer";
import Navbar from "../Navbar/Navbar";

configure({adapter: new Adapter()});

describe("Footer component", () => {
  test("Matches the snapshot with the footer", () => {
    const footer = create(<Footer />);
    expect(footer.toJSON()).toMatchSnapshot();
  });
});

describe("NavBar component", () => {
  test("Matches the snapshot with the navbar", () => {
    const wrapper = create(<MemoryRouter><Navbar /></MemoryRouter>)
    expect(wrapper.toJSON()).toMatchSnapshot();
  });
});
