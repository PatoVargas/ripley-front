import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import MinImages from './MinImages/MinImages';
import Description from './Description/Description';
import CarouselImages from './CarouselImages/CarouselImages';
import Accordion from './Accordion/Accordion';
import axios from 'axios';
import '../App.css';

class ProductDetailComponent extends React.Component{

  state = {
      product: null,
      completed: false,
      error: false
  };

  componentDidMount(){

    function doRequest(props, state) {
      const url = 'https://vast-waters-17110.herokuapp.com/products/';

      axios.get(url+props.match.params.id)
        .then(response => {
          if (! response.data.error) {
            state.setState({product:response.data, completed:true})
          }
          else{
            if (response.data.error.message === 'Error random') {
              console.log("error por el random");
              doRequest(props, state);
            }
            else{
              state.setState({error:true})
              state.props.history.push("/error");
            }
          }
        });
    }

    doRequest(this.props, this);
  };

  render(){

    return (
      <div className="App">
        <Container className="App-header">
        {
          (! this.state.completed && ! this.state.error) ?
          <div>
          <Spinner animation="grow" />
          </div> : null
        }
        <Row>
          <Col xs={1} className="d-none d-md-block d-lg-block d-xl-block">
            { this.state.completed &&
              <MinImages
                images={
                  this.state.product.images.map((image, i) => {
                    return {image:image, key: image};
                  })
                }
                />
            }
          </Col>
          <Col xs={12} md={5}>
            { this.state.completed &&
              <CarouselImages
                images={
                  this.state.product.images.map((image, i) => {
                    return {image:image, key: i};
                  })
                }
                />
            }
          </Col>
          <Col xs={12} md={1} lg={1}></Col>
          <Col xs={12} md={4} lg={4}>
            { this.state.completed &&
              <Description
                product={this.state.product}
                />
            }
          </Col>
        </Row>
        <br></br>
        <hr></hr>
        <Row>
          { this.state.completed &&
            <Accordion
              product={this.state.product}
              />
          }
        </Row>
        </Container>
      </div>
    );
  };
}

export default ProductDetailComponent;
