import React from 'react';

const MinImages = (props) => {

  var images = props.images.map((image) => {
    return(
      <div
       key={image.key}>
      <img
        className="d-block w-100"
        src={image.image}
        key={image.key}
        alt="First slide"
      />
      <br/>
      </div>
    );
  })

  return (
    <div>
      {images}
    </div>
  );
}

export default MinImages;
