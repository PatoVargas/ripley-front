import React from 'react';
import './Description.css';

const Description = (props) => {

  var title_upper = null;
  if (props.product.attributes[0]) {
    title_upper = <h6 className="Grey">{props.product.attributes[0].value}</h6>;
  }

  return (
    <div className="Description">
      {title_upper}
      <h5 className="Black">{props.product.name}</h5>
      <h6 className="GreyMini">SKU: {props.product.partNumber}</h6>
      <br></br>
      <h5 className="Price Grey">Normal <span className="RightSpan">{props.product.prices.formattedListPrice}</span></h5>
      <h5 className="Price Grey"><b>Internet <span className="RightSpan">{props.product.prices.formattedOfferPrice}</span></b></h5>
      <h5 className="Price Red"><b>Tarjeta Ripley
        <span className="RightSpan"><img className="CardImgOffer" src="//static.ripley.cl/images/opex.png" alt="Precio Tarjeta Ripley"/>
        {props.product.prices.formattedCardPrice}</span></b>
      </h5>
      <h5 className="Price Grey"><b>Descuento<span className="RightSpan">-{props.product.prices.discountPercentage}%</span></b></h5>
      <h5 className="Price Purple">Acumulas <span className="RightSpan">{props.product.prices.ripleyPuntos} Ripley Puntos GO</span></h5>
    </div>
  );
}

export default Description;
