import React from 'react';
import Table from 'react-bootstrap/Table';
import './AccordionTable.css';

const AccordionTable = (props) => {

  var rows = props.product.attributes
    .filter(product => product.displayable)
    .map((product, index) => {
      return(
        <tr key={index}>
          <td>{product.name}</td>
          <td>{product.value}</td>
        </tr>
      );
  });

  return (
    <Table className="Table" striped hover responsive>
      <tbody>
        {rows}
      </tbody>
    </Table>
  );
}

export default AccordionTable;
