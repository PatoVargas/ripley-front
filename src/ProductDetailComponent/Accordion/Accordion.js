import React from 'react';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import AccordionTable from './AccordionTable/AccordionTable';
import './Accordion.css';

const AccordionComponent = (props) => {

  function createMarkup() {
    return {__html: props.product.longDescription};
  }

  return (
    <Accordion defaultActiveKey="0">
      <Card>
        <Accordion.Toggle className="CardLeft" as={Card.Header} eventKey="0">
          + DESCRIPCIÓN
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="0">
          <Card.Body  className="CardLeft" dangerouslySetInnerHTML={createMarkup()} ></Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Accordion.Toggle className="CardLeft" as={Card.Header} eventKey="1">
          + ESPECIFICACIONES
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="1">
          <AccordionTable
            product={props.product}
          />
        </Accordion.Collapse>
      </Card>
    </Accordion>
  );
}

export default AccordionComponent;
