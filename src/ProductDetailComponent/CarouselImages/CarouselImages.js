import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import './CarouselImages.css';

const CarouselImages = (props) => {

  var images = props.images.map((image) => {
    return <Carousel.Item
      key={image.key}
    >
      <img
        className="d-block w-100"
        src={image.image}
        key={image.key}
        alt="First slide"
      />
    </Carousel.Item>
  })

  return (
    <Carousel>
      {images}
    </Carousel>
  );
}

export default CarouselImages;
