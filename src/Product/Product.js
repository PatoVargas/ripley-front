import React from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import { Link } from 'react-router-dom';
import './Product.css';

class Product extends React.Component{

  render(){
    let card_offer = null;
    if (this.props.card_price) {
        card_offer = <span className="CardOffer">{this.props.card_price}<img className="CardImgOffer" src="//home.ripley.cl/front/public/images/opex-izq.png" alt="Precio Tarjeta Ripley" /></span>
    }

    let url = '/products/' + this.props.sku
    return (
      <Col md={6} lg={4}>
        <Card className="Card">
          <div>
          <Card.Img className="CardImg" variant="top" src={this.props.full_image} />
          </div>
          <Card.Body>
            <Card.Title className="TitleCard" >{this.props.name}</Card.Title>
            <Card.Text>
              <span className="Normal">Normal: {this.props.original_price}</span><br/>
              <span className="Offer" >Oferta: {this.props.offer_price}</span><br/>
              {card_offer}
            </Card.Text>
          </Card.Body>
          <Card.Footer>
            <Link to={{pathname: url}}>Ver detalle</Link>
          </Card.Footer>
        </Card>
      </Col>
    );
  }
}

export default Product;
