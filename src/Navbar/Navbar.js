import React from 'react';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import PropTypes from "prop-types";
import { Link } from 'react-router-dom'
import { withRouter } from "react-router";

import './Navbar.css';
import logo from '../logo.png';

class NavBar extends React.Component{

  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)
    this.searchHandler = this.searchHandler.bind(this)
    this.changeHandler = this.changeHandler.bind(this)
    this.state = {
        text: '',
        isdisabled: true
    };
  }

  searchHandler = () => {
    this.props.history.replace(`/reload`);
    setTimeout(() => {
      this.props.history.replace(`/products/`+this.state.text);
      this.setState({text:'', isdisabled:true})
    });
  }

  changeHandler = (e) =>{
    var text = e.target.value;
    if (text.length === 0) {
      this.setState({text:text, isdisabled:true})
    }
    else {
      this.setState({text:text, isdisabled:false})
    }
  }

  render(){

    return (
      <div>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand><Link to={{pathname: "/"}}><img src={logo} className="App-logo" alt="logo" /></Link></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
        </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Pega un SKU"
            className="mr-sm-2"
            onChange={this.changeHandler}/>
            <Button id="btn-search" disabled={this.state.isdisabled} onClick={this.searchHandler} variant="info">Buscar</Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
      </div>
    );
  }
}

export default withRouter(NavBar);
