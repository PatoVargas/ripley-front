import React from 'react';
import Navbar from 'react-bootstrap/Navbar'
import './Footer.css';

const footer = () => {
  
  return (
    <div>
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand className="FooterBrand">Patricio Vargas, 2019.</Navbar.Brand>
    </Navbar>
    </div>
  );
}

export default footer;
