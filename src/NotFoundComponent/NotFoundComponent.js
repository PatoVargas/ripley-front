import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import not_found from '../not-found.jpg';
import './NotFoundComponent.css';

const not_found_component = () => {

  return (
    <Row>
      <Col>
        <Image src={not_found} className="NotFound" alt="not-found" fluid />
      </Col>
    </Row>
  );
}

export default not_found_component;
