import React from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from './Navbar/Navbar';
import Footer from './Footer/Footer';
import HomeComponent from './HomeComponent/HomeComponent';
import ProductDetailComponent from './ProductDetailComponent/ProductDetailComponent';
import NotFoundComponent from './NotFoundComponent/NotFoundComponent';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';

class App extends React.Component{

  state = {
      products: []
  };

  render(){

    return (
      <BrowserRouter>
        <div className="App">
          <Navbar/>
          <Container className="App-header">
            <Switch>
            <Route exact path='/' component={HomeComponent} />
            <Route path="/products/:id" component={ProductDetailComponent} />
            <Route component={NotFoundComponent} />
            </Switch>
          </Container>
          <Footer/>
        </div>
      </BrowserRouter>
    );
  };
}

export default App;
