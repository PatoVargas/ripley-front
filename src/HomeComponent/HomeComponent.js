import React from 'react';
import axios from 'axios';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import CardDeck from 'react-bootstrap/CardDeck';
import Product from '../Product/Product';
import '../App.css';

class HomeComponent extends React.Component{

  state = {
      products: []
  };

  componentDidMount(){
    const url = 'https://vast-waters-17110.herokuapp.com/products';
    axios.get(url)
      .then(response => {
        this.setState({products:response.data})
      });
  };

  render(){

    return (
      <div className="App">
        <Container className="App-header">
          <Row>
            <CardDeck>
            {this.state.products.map((product) => {
              return <Product
                sku={product.sku}
                name={product.name}
                full_image={product.full_image}
                original_price={product.original_price}
                card_price={product.card_price}
                offer_price={product.offer_price}
                key={product.id}/>
            })}
            </CardDeck>
          </Row>
        </Container>
      </div>
    );
  };
}

export default HomeComponent;
